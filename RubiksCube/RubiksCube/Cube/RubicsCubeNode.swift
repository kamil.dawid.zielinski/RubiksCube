//
// Created by kamil.zielinski on 02/04/2018.
// Copyright (c) 2018 kamil.zielinski. All rights reserved.
//

import Foundation
import SceneKit



class RubicsCubeNode : SCNNode {
    var cubics : CubicArray<SCNNode>!

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override init()
    {
        super.init()

        let width = CGFloat(1)
        let chamferRadius = CGFloat(0.05)
        let numberOfSegments = 3


        cubics = CubicArray<SCNNode>()
        cubics.array.reserveCapacity(27)
        let coordinate = -CGFloat(numberOfSegments / 2) * width
        for x in 0..<numberOfSegments
        {
            for y in 0..<numberOfSegments
            {
                for z in 0..<numberOfSegments
                {
                    let geometry = SCNBox(width: width, height: width, length: width, chamferRadius: chamferRadius)
                    geometry.materials = MaterialFactory.defaultMaterials()

                    let boxNode = SCNNode(geometry: geometry)
                    boxNode.position = SCNVector3(coordinate + CGFloat(x) * width, coordinate + CGFloat(y) * width, coordinate + CGFloat(z) * width)

                    cubics.add(boxNode)
                    self.addChildNode(boxNode)


                    let threeDimmensionalCoordinate = cubics.get3D(object: boxNode)!
                    let oneDimmensionalCoordinate = cubics.to1D(x: threeDimmensionalCoordinate.x, y: threeDimmensionalCoordinate.y, z: threeDimmensionalCoordinate.z)
                    boxNode.name = "Node (\(oneDimmensionalCoordinate)): (\(threeDimmensionalCoordinate.x), \(threeDimmensionalCoordinate.y), \(threeDimmensionalCoordinate.z))"

                }
            }
        }
    }
}
