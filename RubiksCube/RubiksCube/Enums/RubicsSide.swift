//
// Created by kamil.zielinski on 02/04/2018.
// Copyright (c) 2018 kamil.zielinski. All rights reserved.
//

import Foundation

enum RubicsSide: Int {
    case Front = 0
    case Right
    case Back
    case Left
    case Top
    case Bottom

    static let allValues = [Front, Right, Back, Left, Top, Bottom]
}