//
//  GameViewController.swift
//  RubiksCube
//
//  Created by kamil.zielinski on 06/03/2018.
//  Copyright © 2018 kamil.zielinski. All rights reserved.
//

import UIKit
import QuartzCore
import SceneKit

class GameViewController: UIViewController {
    
    var scnView: SCNView!
    var scnScene: SCNScene!
    var cameraNode : SCNNode!
    var cubeNode: RubicsCubeNode!

    var camRotation: CGVector = CGVector(dx: 0, dy: 0)

    var nodesQueue: Array<SCNHitTestResult>? = nil
    var beginTouches = Set<UITouch>()

    override func viewDidLoad() {
        super.viewDidLoad()

        setupView()
        setupScene()
        setupCamera()
        setupRubicsNode()
    }

    private func setupRubicsNode() {
        cubeNode = RubicsCubeNode()
        scnScene.rootNode.addChildNode(cubeNode)
    }

    func setupView() {
        scnView = self.view as! SCNView
        scnView.isMultipleTouchEnabled = true
    }
    
    func setupScene() {
        
        scnScene = SCNScene()
        scnView.scene = scnScene
    }
    
    func setupCamera() {
        cameraNode = SCNNode()
        cameraNode.camera = SCNCamera()
        cameraNode.position = SCNVector3(0,0,5)

        cameraNode.transform = SCNMatrix4MakeTranslation(0, 0, 5)


        scnScene.rootNode.addChildNode(cameraNode)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)

        self.beginTouches = touches
        if self.beginTouches.count == 1
        {
            nodesQueue = Array<SCNHitTestResult>()
        }
    }

    func invalidateNodesQueue()
    {
        self.fooDiffuseOn()
        self.nodesQueue = nil
    }

    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesMoved(touches, with: event)

        if self.beginTouches.count != touches.count {
            return
        }

        let t = touches.first
        let locationInView = t!.location(in: scnView)

        let previousLocationInView = t!.previousLocation(in: scnView)

        let shouldRotateCamera: Bool = touches.count == 2
        if shouldRotateCamera == true
        {
            rotateCamera(locationInView: locationInView, previousLocationInView: previousLocationInView)
            return
        }

        let hitTestResults = scnView.hitTest(locationInView)
        guard !hitTestResults.isEmpty else {
            return
        }

        guard nodesQueue != nil && nodesQueue!.count < 3 else {
            return
        }

        let bestMatchingResult = self.bestMatchingResult(in: hitTestResults)
        guard bestMatchingResult != nil else {
            return
        }

        let geometryIndex = nodesQueue!.isEmpty ? bestMatchingResult!.geometryIndex : nodesQueue!.first!.geometryIndex
        let geometry: SCNGeometry? = bestMatchingResult!.node.geometry

        geometry!.materials[geometryIndex].diffuse.intensity = 0.2
        nodesQueue!.append(bestMatchingResult!)
    }

    private func bestMatchingResult(`in` results: [SCNHitTestResult]) -> SCNHitTestResult? {
        if nodesQueue!.isEmpty
        {
            return results.first
        }

        var result: SCNHitTestResult? = nil

        let geometryIndex: Int = self.nodesQueue!.first!.geometryIndex
        //TODO: kamil.zielinski Dobre miejsce na sprawdzenie poprawnosci indeksow
        for evaluatedResult in results
        {
            let inQueueObject = nodesQueue!.first { $0.node == evaluatedResult.node }
            if inQueueObject != nil {
                continue
            }

            if geometryIndex != evaluatedResult.geometryIndex
            {
                continue
            }

            return evaluatedResult
        }
        return nil
    }

    private func rotateCamera(locationInView: CGPoint, previousLocationInView: CGPoint) {
        self.camRotation.dx += locationInView.x - previousLocationInView.x
        self.camRotation.dy += locationInView.y - previousLocationInView.y
        //
        let translation = SCNMatrix4MakeTranslation(0, 0, 5)
        var scnMatrix4 = SCNMatrix4Rotate(translation, Float(self.camRotation.dx * 0.01), 0, 1, 0)
        scnMatrix4 = SCNMatrix4Rotate(scnMatrix4, Float(self.camRotation.dy * 0.01), 1, 0, 0)
        cameraNode.transform = scnMatrix4
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)

        guard let unwrappedQueue = nodesQueue else {
            return
        }

        guard unwrappedQueue.count == 3 else
        {
            self.invalidateNodesQueue()
            return
        }

        var xEquals = true
        var yEquals = true
        var zEquals = true

        var xOrdered = true
        var yOrdered = true
        var zOrdered = true

        for (i, v) in nodesQueue!.enumerated()
        {
            if i == 0 {
                continue
            }

            let evaluatedNode = v.node
            let previousNode = nodesQueue![i-1].node

            let evaluatedCoordinates = cubeNode.cubics.get3D(object: evaluatedNode)!
            let previousCoordinates = cubeNode.cubics.get3D(object: previousNode)!

            ///Check order
            if xOrdered == true && previousCoordinates.x > evaluatedCoordinates.x {
                xOrdered = false
            }
            if yOrdered == true && previousCoordinates.y > evaluatedCoordinates.y {
                yOrdered = false
            }

            if zOrdered == false && previousCoordinates.z > evaluatedCoordinates.z {
                zOrdered = false
            }

            /// Check equality
            if(xEquals == true && evaluatedCoordinates.x != previousCoordinates.x) {
                xEquals = false
            }
            if(yEquals == true && evaluatedCoordinates.y != previousCoordinates.y) {
                yEquals = false
            }
            if(zEquals == true && evaluatedCoordinates.z != previousCoordinates.z) {
                zEquals = false
            }
        }


        let coordinates = cubeNode.cubics.get3D(object: nodesQueue!.first!.node)!

        for i in 0..<3 {
            for j in 0..<3
            {
                var optionalNode: SCNNode? = nil
                if yEquals && zEquals {
                    optionalNode = cubeNode.cubics.get(x: i, y: coordinates.y, z: j)
                }
                if xEquals && yEquals {
                    optionalNode = cubeNode.cubics.get(x: coordinates.x, y: i, z: j)
                }
                if xEquals && zEquals {
                    optionalNode = cubeNode.cubics.get(x: coordinates.x, y: i, z: j)
                }



                guard let node = optionalNode else {
                    continue
                }

                var copy = Array(node.geometry!.materials)
                var newMaterials = node.geometry!.materials
                if yEquals && zEquals
                {
                    if xOrdered
                    {
                        newMaterials[0] = copy[3]
                        newMaterials[1] = copy[0]
                        newMaterials[2] = copy[1]
                        newMaterials[3] = copy[2]
                    }
                    else
                    {
                        newMaterials[0] = copy[1]
                        newMaterials[1] = copy[2]
                        newMaterials[2] = copy[3]
                        newMaterials[3] = copy[0]
                    }
                }
                if xEquals && yEquals
                {
                    if zOrdered
                    {
                        newMaterials[0] = copy[5]
                        newMaterials[2] = copy[4]
                        newMaterials[4] = copy[0]
                        newMaterials[5] = copy[2]
                    }
                    else
                    {
                        newMaterials[0] = copy[4]
                        newMaterials[2] = copy[5]
                        newMaterials[4] = copy[2]
                        newMaterials[5] = copy[0]
                    }
                }
                if xEquals && zEquals
                {
                    if yOrdered
                    {
                        newMaterials[0] = copy[5]
                        newMaterials[2] = copy[4]
                        newMaterials[4] = copy[0]
                        newMaterials[5] = copy[2]
                    }
                    else
                    {
                        newMaterials[0] = copy[4]
                        newMaterials[2] = copy[5]
                        newMaterials[4] = copy[2]
                        newMaterials[5] = copy[0]
                    }
                }
                node.geometry!.materials = newMaterials
            }
        }

//        for hitResult in nodesQueue!
//        {
//            let node: SCNNode = hitResult.node
//            let geometryIndex = hitResult.geometryIndex


//            let xRotation: Float = rotation.y == 0 || rotation.z == 0 ? 1 : 0

//            node.geometry!.materials = newMaterials
//            node.geometry!.materials[geometryIndex].diffuse.contents = UIColor.black




//            hitResult.node.geometry!.materials[hitResult.geometryIndex].diffuse.contents = UIColor.yellow


//        }
        self.invalidateNodesQueue()
    }

    private func fooDiffuseOn() {
        if nodesQueue == nil {
            return
        }

        for hitResult in nodesQueue!
        {
            let node: SCNNode = hitResult.node
            for material in node.geometry!.materials {
                material.diffuse.intensity = 1.0
            }
        }
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
    }

    override func touchesEstimatedPropertiesUpdated(_ touches: Set<UITouch>) {
        super.touchesEstimatedPropertiesUpdated(touches)
    }
}
