//
// Created by kamil.zielinski on 02/04/2018.
// Copyright (c) 2018 kamil.zielinski. All rights reserved.
//

import Foundation
import SceneKit

class MaterialFactory
{
    private static let defaultColors: [RubicsSide : UIColor] = [
        .Front : UIColor.red,
        .Right : UIColor.blue,
        .Back : UIColor.orange,
        .Left : UIColor.green,
        .Top : UIColor.white,
        .Bottom : UIColor.yellow
    ]

    static func defaultMaterials() -> [SCNMaterial]
    {
        var materials = [SCNMaterial]()
        for side in RubicsSide.allValues {
            let material = SCNMaterial()
            material.diffuse.contents = defaultColors[side]

            materials.append(material)
        }
        return materials
    }

}
