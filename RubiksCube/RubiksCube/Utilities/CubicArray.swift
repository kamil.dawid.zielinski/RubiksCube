//
// Created by kamil.zielinski on 02/04/2018.
// Copyright (c) 2018 kamil.zielinski. All rights reserved.
//

import Foundation

class CubicArray<T>
{
    var array: [T]! = [T]()

    func to1D(x: Int, y: Int, z: Int) -> Int
    {
        let result: Int = x * (9) + y * 3 + z
        return result
    }

    func to3D(coordinate: Int) -> (x: Int, y:Int, z:Int)
    {
        let squaredSegmentsCount: Int = 9
        let x = coordinate / squaredSegmentsCount

        var subtracted: Int = coordinate - (x * squaredSegmentsCount)
        let y = subtracted / 3

        subtracted = subtracted - (y * 3)
        let z = subtracted

        return (x,y,z)
    }

    var flattenArray: [T]! {
        get {
            return self.array
        }
    }

    func add(_ object: T)
    {
        self.array.append(object)
    }

    func get3D(object: T) -> (x: Int, y:Int, z:Int)? {
        let optionalIndex = self.array.index { (v: T) in (v as AnyObject) === (object as AnyObject) }
        guard let idx = optionalIndex else {
            return nil
        }

        return to3D(coordinate: idx)
    }

    func get(x: Int, y: Int, z: Int) -> T
    {
        let idx = to1D(x: x, y: y, z: z)
        return array[idx]
    }

}
