Projekt zaliczeniowy przedmiotu zaawansowane programowanie gier.

Celem projektu jest napisanie trojwymiarowej kostki rubika na urzadzenia typu iPad. Do napisania tej aplikacji zostanie wykorzystany jezyk Swift 4.x z technologia SceneKit. 


Kamienie milowe projektu:

1. Zapoznanie z API SceneKit. Utworzenie projektu i repozytorium.
2. Implementacja wizualna kostki
3. Implementacja interakcji uzytkownika
